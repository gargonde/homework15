// Homework15.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>

//input parametrs:
// bool IsOdd - if true print Odd numbers else print Even numbers
// int N - last number in sequence from 0 to N
void printOddOrEvenNumbers(bool IsOdd, int N) 
{
	for (int i = IsOdd; i <= N; i = i + 2)
	{
		std::cout << i << '\n';
	}
}

int main()
{
	int N = 13;
	std::cout << "MAIN. Even numbers from 0 to " << N << '\n';
	for (int i = 0; i <= N; i = i + 2)
	{
		std::cout << i << '\n';
	}
	std::cout << "Function. Even numbers from 0 to " << N << '\n';
	printOddOrEvenNumbers(false, N);
	std::cout << "Function. Odd numbers from 0 to " << N << '\n';
	printOddOrEvenNumbers(true, N);
}